WIFI_locator:server_tcp.o
	cc -o server_tcp -g server_tcp.o -I /usr/include/libxml2 -L /usr/lib -lxml2 -lm
	
server_tcp.o:server_tcp.c
	cc -c -g server_tcp.c -I /usr/include/libxml2 -L /usr/lib -lxml2 -lm

board:
	cc -o server_tcp -DBOARD -g server_tcp.c -I /usr/include/libxml2 -L /usr/lib -lxml2 -lm


clean:
	rm server_tcp server_tcp.o wifiinfo.xml
